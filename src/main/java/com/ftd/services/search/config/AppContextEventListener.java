package com.ftd.services.search.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.stereotype.Component;

@Component
public class AppContextEventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppContextEventListener.class);

    private boolean             enabled;

    public AppContextEventListener(
            @Value("${spring.logProperties:true}") boolean enabled) {
        this.enabled = enabled;
    }

    @EventListener
    public void handleContextRefreshed(ContextRefreshedEvent event) {
        if (enabled) {
            printActiveProperties((ConfigurableEnvironment) event.getApplicationContext().getEnvironment());
        } else {
            LOGGER.info("************************* spring.logProperties=false");
        }
    }

    private void printActiveProperties(ConfigurableEnvironment env) {
        logConfigGroup("Host Info", "springCloudClientHostInfo", env);
        logConfigGroup("Server/Client", "configServerClient", env);
        logConfigGroup("active properties", "applicationConfig", env);
        logConfigGroup("System", "system", env);
    }

    void logConfigGroup(String title, String pattern, ConfigurableEnvironment env) {

        List<MapPropertySource> propertySources = new ArrayList<>();

        env.getPropertySources().forEach(it -> {
            if (it instanceof MapPropertySource && it.getName().contains(pattern)) {
                propertySources.add((MapPropertySource) it);
            }
        });

        LOGGER.info("************************* {}", title);
        propertySources.stream()
                .map(propertySource -> propertySource.getSource().keySet())
                .flatMap(Collection::stream)
                .distinct()
                .sorted()
                .forEach(key -> {
                    try {
                        LOGGER.info("{} -> {}", key, env.getProperty(key));
                    } catch (Exception e) {
                        LOGGER.info("{} -> {}", key, e.getMessage());
                    }
                });
    }
}
