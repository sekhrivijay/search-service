package com.ftd.services.search.bl.clients.availibility;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.ftd.services.search.api.request.SearchServiceRequest;
import com.ftd.services.search.api.response.SearchServiceResponse;
import com.ftd.services.search.bl.clients.BaseClient;
import com.ftd.services.search.api.GlobalConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

/**
 * https://tools.publicis.sapient.com/confluence/display/FLTD/ProductAvailability
 *
 * @author cdegreef
 */
@Named("availabilityClient")
public class AvailabilityClientImpl extends BaseClient implements AvailabilityClient {
    private static final Logger                     LOGGER                         = LoggerFactory
            .getLogger(AvailabilityClientImpl.class);
    public static final AvailabilityServiceResponse FALLBACK_AVAILABILITY_RESPONSE = new AvailabilityServiceResponse();
    public static final AvailabilityServiceResponse DUMMY_AVAILABILITY_RESPONSE    = new AvailabilityServiceResponse();

    private RestTemplate                            restTemplate;
    private String                                  baseUrl;

    @Value("${service.availabilityService.enabled:true}")
    private boolean                                 enabled                        = false;

    @Value("${service.availabilityService.version:0.1}")
    private String                                  version;

    public AvailabilityClientImpl(
            @Autowired RestTemplate restTemplate,
            @Value("${service.availabilityService.baseUrl}") String baseUrl) {

        this.restTemplate = restTemplate;
        this.baseUrl = baseUrl;
    }

    public Map<String, Object> buildMap(AvailabilityServiceResponse availability, Set<String> setOfExpectedProductIds) {
        Map<String, Object> results = new HashMap<>();
        /*
         * First populate the resulting dictionary by product with unavailable instances
         * just in case we don't get a specific product back from the availability
         * service.
         */
        setOfExpectedProductIds.forEach(pid -> results.put(pid, new AvailabilitySearchResponse(true)));

        if (availability.getProductAvailabilities() != null) {
            try {
                /*
                 * The JSON string is an array of "product" instances. We want to figure out the
                 * id for each of them and put the id as the key with the product object being
                 * the value.
                 */
                availability.getProductAvailabilities()
                        .forEach(pa -> pa.getProductIds()
                                .forEach(pid -> results.put(pid, new AvailabilitySearchResponse(pa.isAvailable()))));
            } catch (Exception e) {
                LOGGER.warn("{}", e.getMessage());
            }
        }
        return results;

    }

    /**
     * Visibility for jUnit testing, otherwise consider to be private.
     *
     * @param productIds
     * @param startDate
     * @param endDate
     * @param zipCode
     * @return
     */
    public String buildUniquePartOfUrl(
            Set<String> productIds,
            String startDate,
            String endDate,
            String zipCode) {

        AvailabilityParms ap = new AvailabilityParms();

        if (productIds != null) {
            productIds.stream().forEach(id -> ap.addProductId(0, id));
        }
        if (startDate != null || endDate != null) {
            ap.addDateRange(startDate, endDate);
        }
        ap.setDeliveryZipCode(zipCode);

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        StringBuilder url = new StringBuilder();
        url.append(gson.toJson(ap));
        return url.toString();
    }

    @Override
    @Timed
    @ExceptionMetered
    @HystrixCommand(groupKey = "hystrixGroup",
            commandKey = "availabilityServiceKey",
            threadPoolKey = "availabilityThreadPoolKey",
            fallbackMethod = "callAvailabilityServiceFallback")
    public AvailabilityServiceResponse callAvailabilityService(
            SearchServiceRequest searchServiceRequest,
            SearchServiceResponse searchServiceResponse) {

        if (!enabled) {
            return DUMMY_AVAILABILITY_RESPONSE;
        }

        String startDate = searchServiceRequest.getAvailFrom();
        String endDate = searchServiceRequest.getAvailTo();
        String zipCode = searchServiceRequest.getZipCode();

        Set<String> productIds = getPids(searchServiceResponse);

        String uniquePartOfUrl = buildUniquePartOfUrl(productIds, startDate, endDate, zipCode);
        StringBuilder fullUrl = new StringBuilder();
        fullUrl.append(baseUrl.replace(GlobalConstants.SITE_ID, searchServiceRequest.getSiteId()));

        HttpEntity<AvailabilityServiceResponse> entity = new HttpEntity<>(createHttpHeaders(version));
        ResponseEntity<AvailabilityServiceResponse> response = null;
        try {
            if (uniquePartOfUrl != null && uniquePartOfUrl.trim().length() > 0) {
                fullUrl.append(GlobalConstants.QUESTION_MARK);
                fullUrl.append("params={paramsVariable}");
            }

            response = restTemplate.exchange(
                    fullUrl.toString(),
                    HttpMethod.GET,
                    entity,
                    AvailabilityServiceResponse.class,
                    uniquePartOfUrl.toString());

            if (response.getStatusCode() != HttpStatus.OK) {
                return callAvailabilityServiceFallback(searchServiceRequest, searchServiceResponse);
            }
            return response.getBody();

        } catch (Exception e) {
            return callAvailabilityServiceFallback(searchServiceRequest, searchServiceResponse);
        }
    }

    public AvailabilityServiceResponse callAvailabilityServiceFallback(
            SearchServiceRequest searchServiceRequest,
            SearchServiceResponse searchServiceResponse) {
        return FALLBACK_AVAILABILITY_RESPONSE;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
