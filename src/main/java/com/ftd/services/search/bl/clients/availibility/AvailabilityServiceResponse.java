package com.ftd.services.search.bl.clients.availibility;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AvailabilityServiceResponse {

    public static class ProductAvailability {

        @SerializedName("productIds")
        @Expose
        private List<String> productIds      = new ArrayList<>();;
        @SerializedName("isAvailable")
        @Expose
        private Boolean       isAvailable;
        @SerializedName("datesAvailable")
        @Expose
        private List<String>  datesAvailable  = new ArrayList<>();;
        @SerializedName("zipCode")
        @Expose
        private String        zipCode;

        public List<String> getProductIds() {
            return productIds;
        }

        public void setProductIds(List<String> productIds) {
            this.productIds = productIds;
        }

        public Boolean isAvailable() {
            return isAvailable;
        }

        public void setIsAvailable(Boolean isAvailable) {
            this.isAvailable = isAvailable;
        }

        public List<String> getDatesAvailable() {
            return datesAvailable;
        }

        public void setDatesAvailable(List<String> datesAvailable) {
            this.datesAvailable = datesAvailable;
        }

        public String getZipCode() {
            return zipCode;
        }

        public void setZipCode(String zipCode) {
            this.zipCode = zipCode;
        }

    }

    @SerializedName("productAvailabilities")
    @Expose
    private List<ProductAvailability> productAvailabilities = new ArrayList<>();

    public List<ProductAvailability> getProductAvailabilities() {
        return productAvailabilities;
    }

    public void setProductAvailabilities(List<ProductAvailability> productAvailabilities) {
        this.productAvailabilities = productAvailabilities;
    }

}
