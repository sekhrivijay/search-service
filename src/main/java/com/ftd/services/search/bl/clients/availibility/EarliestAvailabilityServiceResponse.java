package com.ftd.services.search.bl.clients.availibility;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EarliestAvailabilityServiceResponse {

    public static class ProductEarliestAvailability {

        @SerializedName("productIds")
        @Expose

        private List<String> productIds = null;
        @SerializedName("isAvailable")
        @Expose
        private Boolean      isAvailable;

        @SerializedName("earliestAvailableDate")
        @Expose
        private String       earliestAvailableDate;

        public List<String> getProductIds() {
            return productIds;
        }

        public void setProductIds(List<String> productIds) {
            this.productIds = productIds;
        }

        public Boolean getIsAvailable() {
            return isAvailable;
        }

        public void setIsAvailable(Boolean isAvailable) {
            this.isAvailable = isAvailable;
        }

        public String getEarliestAvailableDate() {
            return earliestAvailableDate;
        }

        public void setEarliestAvailableDate(String earliestAvailableDate) {
            this.earliestAvailableDate = earliestAvailableDate;
        }

    }

    public static class ProductsEarliestAvailabilityResponse {

        @SerializedName("productEarliestAvailabilities")
        @Expose
        private List<ProductEarliestAvailability> productEarliestAvailabilities = null;

        public List<ProductEarliestAvailability> getProductEarliestAvailabilities() {
            return productEarliestAvailabilities;
        }

        public void setProductEarliestAvailabilities(List<ProductEarliestAvailability> productEarliestAvailabilities) {
            this.productEarliestAvailabilities = productEarliestAvailabilities;
        }

    }

    @SerializedName("productsEarliestAvailabilityResponse")
    @Expose
    private ProductsEarliestAvailabilityResponse productsEarliestAvailabilityResponse;

    public ProductsEarliestAvailabilityResponse getProductsEarliestAvailabilityResponse() {
        return productsEarliestAvailabilityResponse;
    }

    public void setProductsEarliestAvailabilityResponse(
            ProductsEarliestAvailabilityResponse productsEarliestAvailabilityResponse) {
        this.productsEarliestAvailabilityResponse = productsEarliestAvailabilityResponse;
    }

}
