package com.ftd.services.search.bl.clients.availibility;

import java.util.Map;

import com.ftd.services.search.api.request.SearchServiceRequest;
import com.ftd.services.search.api.response.SearchServiceResponse;

public interface EarliestAvailabilityClient {

    EarliestAvailabilityServiceResponse callAvailabilityService(
            SearchServiceRequest searchServiceRequest,
            SearchServiceResponse searchServiceResponse);

    Map<String, Object> buildMap(EarliestAvailabilityServiceResponse products);
}
