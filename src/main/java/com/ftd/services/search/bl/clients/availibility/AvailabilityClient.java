package com.ftd.services.search.bl.clients.availibility;

import java.util.Map;
import java.util.Set;

import com.ftd.services.search.api.request.SearchServiceRequest;
import com.ftd.services.search.api.response.SearchServiceResponse;

public interface AvailabilityClient {

    AvailabilityServiceResponse callAvailabilityService(
            SearchServiceRequest searchServiceRequest,
            SearchServiceResponse searchServiceResponse);

    Map<String, Object> buildMap(AvailabilityServiceResponse products, Set<String> expectedProductIds);
}
