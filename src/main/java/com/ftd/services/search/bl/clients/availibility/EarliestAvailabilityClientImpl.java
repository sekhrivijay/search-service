package com.ftd.services.search.bl.clients.availibility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.ftd.services.search.api.request.SearchServiceRequest;
import com.ftd.services.search.api.response.SearchServiceResponse;
import com.ftd.services.search.bl.clients.BaseClient;
import com.ftd.services.search.api.GlobalConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

/**
 *
 * @author cdegreef
 */
@Named("earliestAvailabilityClient")
public class EarliestAvailabilityClientImpl extends BaseClient implements EarliestAvailabilityClient {
    private static final Logger                      LOGGER  = LoggerFactory
            .getLogger(EarliestAvailabilityClientImpl.class);
    static final EarliestAvailabilityServiceResponse FALLBACK_AVAILABILITY_RESPONSE;
    static final EarliestAvailabilityServiceResponse DUMMY_AVAILABILITY_RESPONSE;
    static final SimpleDateFormat                    EARLIEST_DATE_FORMAT;

    private RestTemplate                             restTemplate;
    private String                                   baseUrl;

    @Value("${service.earliestAvailabilityService.enabled:true}")
    private boolean                                  enabled = false;

    @Value("${service.earliestAvailabilityService.version:0.1}")
    private String                                   version;

    static {
        FALLBACK_AVAILABILITY_RESPONSE = new EarliestAvailabilityServiceResponse();
        DUMMY_AVAILABILITY_RESPONSE = new EarliestAvailabilityServiceResponse();
        EARLIEST_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    }

    public EarliestAvailabilityClientImpl(
            @Autowired RestTemplate restTemplate,
            @Value("${service.earliestAvailabilityService.baseUrl}") String baseUrl) {
        this.restTemplate = restTemplate;
        this.baseUrl = baseUrl;
    }

    Date parseEarliestDate(String earliestAvailableDate) {
        try {
            return EARLIEST_DATE_FORMAT.parse(earliestAvailableDate);
        } catch (ParseException e) {
            LOGGER.warn("earliest availabilities service returned a bad date: {}", earliestAvailableDate);
            return null;
        }
    }

    public Map<String, Object> buildMap(EarliestAvailabilityServiceResponse availability) {
        Map<String, Object> results = new HashMap<>();

        if (availability.getProductsEarliestAvailabilityResponse() != null) {
            try {
                /*
                 * The JSON string is an array of "product" instances. We want to figure out the
                 * id for each of them and put the id as the key with the product object being
                 * the value.
                 */
                availability.getProductsEarliestAvailabilityResponse().getProductEarliestAvailabilities()
                        .forEach(pa -> pa.getProductIds()
                                .forEach(pid -> results.put(pid, new AvailabilitySearchResponse(
                                        parseEarliestDate(pa.getEarliestAvailableDate())))));
            } catch (Exception e) {
                LOGGER.warn("{}", e.getMessage());
            }
        }
        return results;

    }

    /**
     * Visibility for jUnit testing, otherwise consider to be private.
     *
     * @param productIds
     * @param startDate
     * @param endDate
     * @param zipCode
     * @return
     */
    public String buildUniquePartOfUrl(Set<String> productIds) {

        AvailabilityParms ap = new AvailabilityParms();

        if (productIds != null) {
            productIds.stream().forEach(id -> ap.addProductId(0, id));
        }

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        StringBuilder url = new StringBuilder();
        url.append(gson.toJson(ap));
        return url.toString();
    }

    @Override
    @Timed
    @ExceptionMetered
    @HystrixCommand(groupKey = "hystrixGroup",
            commandKey = "earliestAvailabilityServiceKey",
            threadPoolKey = "earliestAvailabilityThreadPoolKey",
            fallbackMethod = "callEarliestAvailabilityServiceFallback")
    public EarliestAvailabilityServiceResponse callAvailabilityService(
            SearchServiceRequest searchServiceRequest,
            SearchServiceResponse searchServiceResponse) {

        if (!enabled) {
            return DUMMY_AVAILABILITY_RESPONSE;
        }

        Set<String> productIds = getPids(searchServiceResponse);

        String uniquePartOfUrl = buildUniquePartOfUrl(productIds);
        StringBuilder fullUrl = new StringBuilder();
        fullUrl.append(baseUrl.replace(GlobalConstants.SITE_ID, searchServiceRequest.getSiteId()));

        HttpEntity<EarliestAvailabilityServiceResponse> entity = new HttpEntity<>(createHttpHeaders(version));
        ResponseEntity<EarliestAvailabilityServiceResponse> response = null;
        try {
            if (uniquePartOfUrl != null && uniquePartOfUrl.trim().length() > 0) {
                fullUrl.append(GlobalConstants.QUESTION_MARK);
                fullUrl.append("params={paramsVariable}");
            }

            response = restTemplate.exchange(
                    fullUrl.toString(),
                    HttpMethod.GET,
                    entity,
                    EarliestAvailabilityServiceResponse.class,
                    uniquePartOfUrl.toString());

            if (response.getStatusCode() != HttpStatus.OK) {
                return callEarliestAvailabilityServiceFallback(searchServiceRequest, searchServiceResponse);
            }
            return response.getBody();

        } catch (Exception e) {
            return callEarliestAvailabilityServiceFallback(searchServiceRequest, searchServiceResponse);
        }
    }

    public EarliestAvailabilityServiceResponse callEarliestAvailabilityServiceFallback(
            SearchServiceRequest searchServiceRequest,
            SearchServiceResponse searchServiceResponse) {
        return FALLBACK_AVAILABILITY_RESPONSE;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
