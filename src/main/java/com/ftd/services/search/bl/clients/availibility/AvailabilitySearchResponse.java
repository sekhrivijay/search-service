package com.ftd.services.search.bl.clients.availibility;

import java.util.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AvailabilitySearchResponse {

    @SerializedName("isAvailable")
    @Expose
    private boolean available;

    @SerializedName("earliestAvailable")
    @Expose
    private Date    earliestAvailable;

    public AvailabilitySearchResponse(boolean available) {
        this.available = available;
    }

    public AvailabilitySearchResponse(Date earliestAvailable) {
        this.earliestAvailable = earliestAvailable;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Date getEarliestAvailable() {
        return earliestAvailable;
    }

    public void setEarliestAvailable(Date earliestAvailable) {
        this.earliestAvailable = earliestAvailable;
    }
}
